(ns shadow-cljs
  (:require [integrant.core :as ig]
            [shadow.cljs.devtools.server :refer [start! stop!]]
            [shadow.cljs.devtools.api :refer [watch]]))

(defmethod ig/init-key ::server [_ _] (start!))
(defmethod ig/suspend-key! ::server [_ _] nil)
(defmethod ig/resume-key ::server [_ _ _ old-impl] old-impl)
(defmethod ig/halt-key! ::server [_ _] (stop!))

(defmethod ig/init-key ::watch [_ {:keys [build-id]}] (watch build-id))
(defmethod ig/suspend-key! ::watch [_ _] nil)
(defmethod ig/resume-key ::watch [_ _ _ old-impl] old-impl)
(defmethod ig/halt-key! ::watch [_ _] (stop!))

