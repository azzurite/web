#!/usr/bin/env bash

clj-kondo --config-dir .clj-kondo --lint "$(clojure -Spath)" --dependencies --parallel --copy-configs
