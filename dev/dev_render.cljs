(ns dev-render
  (:require
    [azzurite.web.frontend.core :as azzu]
    [cljs.spec.test.alpha :as stest]))

(defn render []
  (stest/instrument)
  (azzu/render))
