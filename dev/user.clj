(ns user
  (:require [integrant.repl :as ig-repl :refer [clear halt prep init]]
            [meta-merge.core :refer [meta-merge]]
            [azzurite.web.config :refer [read-config]]
            [integrant.core :as ig]
            [clojure.spec.test.alpha :as stest]))


(integrant.repl/set-prep! (fn []
                            (let [config (meta-merge
                                           (read-config "config.edn")
                                           (read-config "dev-config.edn"))]
                              (ig/load-namespaces config)
                              config)))

(defn go []
  (ig-repl/go)
  (stest/instrument))
(defn reset []
  (ig-repl/reset)
  (stest/instrument))
(defn reset-all []
  (ig-repl/reset-all)
  (stest/instrument))
