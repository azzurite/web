Currently, the purpose of the website is only to show what I do software-wise, no other personal things. That may change in the future.

# Developing

* `npm install`
* `clj -A:dev:cljs` (Run a REPL with aliases `cljs` and `dev`)
* Enter `(go)` to start the system. 
* Cljs reloading will happen automatically, clj reloading with `(reset)`
* Optionally connect to (automatically started) cljs REPL on the port in `.nrepl-port`

# Deploying to Production

Still to do.
