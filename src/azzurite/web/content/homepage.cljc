(ns azzurite.web.content.homepage
  (:refer-clojure :exclude [rem]) ; is now a CSS unit
  (:require [rum.core :as rum :refer [reactive]]
            [stylefy.core :as stylefy :refer [use-style]]
            [garden.units :refer [px vh em rem]]
            [garden.color :as color :refer [rgb]]
            [garden.arithmetic :as g]
            [medley.core :refer [filter-keys]]
            [clojure.spec.alpha :as s]))

(def colors {::accent (rgb 255 201 117)
             ::background (rgb 29 34 59)
             ::background-accent (rgb 45 49 71)
             ::text (rgb 242 239 230)})
(def max-mobile-device-width (px 768)) ; do not change this unless there's a very good reason and check all usages
(def min-desktop-breakpoint (g/+ 1 max-mobile-device-width))
(def only-on-mobile {::stylefy/media {{:min-width min-desktop-breakpoint}
                                      {:display "none"}}})
(def only-on-desktop {:display "none"
                      ::stylefy/media {{:min-width min-desktop-breakpoint}
                                       {:display "unset"}}})

(defn accent-color [text-element]
  [:span (use-style {:color (::accent colors)}) text-element])

(s/def ::rum-markup vector?)
(s/def ::rum-element #?(:cljs (s/or
                                :react #(js/React.isValidElement %)
                                :markup ::rum-markup)
                        :clj ::rum-markup))

(declare page)
(s/fdef page
  :args (s/cat :contents ::rum-element)
  :ret ::rum-element)
(rum/defc page < reactive [contents]
  [:div (use-style {:min-height (vh 100)
                    :background (::background colors)
                    :color (::text colors)
                    :overflow "auto"
                    :font-family "Verdana, sans-serif"})
   contents])

(declare container)
(s/def ::styles map?)
(s/def ::attrs map?)
(s/fdef container
  :args (s/alt
          :one-arg (s/cat :contents ::rum-element)
          :two-args (s/cat :styles ::styles
                      :contents ::rum-element)
          :three-args (s/cat :styles ::styles
                        :attrs ::attrs
                        :contents ::rum-element))
  :ret ::rum-element)

(rum/defc container < reactive
  ([contents]
   (container {} {} contents))
  ([styles contents]
   (container styles {} contents))
  ([styles attrs contents]
   (let [default-styles {:max-width (rem 60)
                         ;; Split up to be able to override individual values
                         :padding-top "0"
                         :padding-bottom "0"
                         :padding-left (rem 0.4)
                         :padding-right (rem 0.4)
                         :margin "auto"
                         ::stylefy/media {{:min-width min-desktop-breakpoint}
                                          {:padding-left (rem 1)
                                           :padding-right (rem 1)}}}]
     [:div (use-style (merge default-styles styles) attrs)
      contents])))

(rum/defc container-accented
  ([contents]
   (container-accented {} {} contents))
  ([styles contents]
   (container-accented styles {} contents))
  ([styles attrs contents]
   (let [default-styles {:background-color (::background-accent colors)
                         :padding-top      (rem 1)
                         :padding-bottom   (rem 1)
                         :margin-top       (rem 3)
                         :margin-bottom    (rem 3)
                         ::stylefy/manual  [[:>:first-child {:margin-top (px 0)}]
                                            [:>:last-child {:margin-bottom (px 0)}]]}]
     (container (merge default-styles styles) attrs contents))))


(declare link)
(s/def ::url string?)
(s/def ::in-new-tab boolean?)
(s/fdef link
  :args (s/alt
          :no-opts (s/cat
                     :url ::url
                     :text string?)
          :full (s/cat
                  :url ::url
                  :text string?
                  :options (s/keys :opt-un [::in-new-tab])))
  :ret ::rum-element)
(rum/defc link < reactive
  ([url text]
   (link url text {}))
  ([url text options]
   (let [optional-attrs {:in-new-tab {:target "_blank"}}
         options-keyset (into #{} (keys options))
         extra-attrs (->> optional-attrs
                       (filter-keys options-keyset)
                       (vals)
                       (apply merge))]
     [:a (use-style
           {:color (color/lighten (::accent colors) 16)}
           (merge extra-attrs {:href url}))
      text])))


(declare media-min-width-styles)
(s/def ::dimension (s/or :raw-value string?
                     :garden-unit garden.units/length?))
(s/fdef media-min-width-styles
  :args (s/+ (s/cat :breakpoint ::dimension
               :styles ::styles)))
(defn media-min-width-styles
  [& style-definitions]
  (-> (reduce
        (fn [acc [dimension style]]
          (let [prev (::prev acc)]
            (-> (if prev
                  (assoc acc {:min-width dimension
                              :max-width prev} style)
                  (assoc acc {:min-width dimension} style))
              (assoc ::prev dimension))))
        {}
        ;; the last one is open-ended, so reversing allows us to initialize the max for the range(s) below
        (reverse (partition 2 style-definitions)))
    (dissoc ::prev)))


(rum/defc mission-statement < reactive []
  [:<>
   [:p
    "I absolutely love playing Dungeons & Dragons and other tabletop RPGs! I want to maximize "
    "the potential of this amazing hobby and make it even greater than it already is."]
   [:p
    "Since I'm a software developer and also love doing that, I want to achieve this goal by creating "
    "tools that help you improve your experience while playing."]
   [:p "Please consider "]])


(rum/defc welcome < reactive []
  (let [full-width-wrapper-style {:display             "flex"
                                  :align-items         "center"
                                  :background-image    "url(img/me.jpg)"
                                  :background-size     "250%"
                                  :min-height          (px 250)
                                  :background-position "58% 20%"
                                  :background-repeat   "no-repeat"
                                  ::stylefy/media      (media-min-width-styles
                                                         (px 400)
                                                         {:min-height (px 300)}
                                                         (px 500)
                                                         {:min-height      (px 325)
                                                          :background-size "210%"}
                                                         (px 600)
                                                         {:min-height      (px 325)
                                                          :background-size "180%"}
                                                         min-desktop-breakpoint
                                                         {:min-height      (px 375)
                                                          :background-size "160%"}
                                                         (px 900)
                                                         {:min-height      (px 375)
                                                          :background-size "120%"}
                                                         (px 1200)
                                                         {:min-height      (px 425)
                                                          :background-size "100%"}
                                                         (px 1600)
                                                         {:min-height      (px 550)
                                                          :background-size "100%"}
                                                         (px 2000)
                                                         {:min-height      (px 700)
                                                          :background-size "cover"})}
        content-div [:div (use-style {:position "relative"
                                      :z-index  "1"})
                     [:h1 (use-style {:font-size (em 1.5)})
                      (accent-color "Welcome. ")
                      [:br]
                      "I create software tools for Dungeons & Dragons."]
                     [:div (use-style only-on-desktop)
                      (mission-statement)]]
        background-div [:div (use-style {:position         "absolute"
                                         :top              (rem -1)
                                         :right            (rem -1)
                                         :bottom           (rem -1)
                                         :left             (rem -1)
                                         :background-color (::background colors)})]]
    [:<>
     [:div (use-style full-width-wrapper-style)
      (container
        [:div (use-style {:width    "50%"
                          :position "relative"})
         content-div
         background-div])]
     (container only-on-mobile
       (mission-statement))]))

(rum/defc video-embed < reactive [url]
  [:video (use-style {:width "100%"}
            {:src url
             :controls "controls"})])

(rum/defc heading-with-accent < reactive [level accent text]
  [level
   [:div (use-style {:font-size (em 0.6)
                     :color (::accent colors)})
    accent]
   text])


(rum/defc mapping-dragons-showcase < reactive []
  (container-accented
    [:<>
     (heading-with-accent :h2 "Website" "Mapping Dragons")
     [:p
      "The problem with searching for maps online, until now, was not being able to find what you want: people "
      "post maps all over the internet, but they never call it \"desert hut in sandstorm\" but things like "
      "\"Valorian The Rogue's hideout\" - Google will not help there."]
     [:p
      "Mapping Dragons aims to rectify that by not only providing a common directory where everyone can upload "
      "their maps, but also providing a system that makes sure that maps are easily searchable."]]))

(rum/defc module-showcase < reactive [name contents]
  [:div (use-style {:margin-top (rem 3)})
   (heading-with-accent :h3 "Module" name)
   contents])


(rum/defc foundry-pings-showcase < reactive []
  (module-showcase "Pings"
    [:<>
     [:p "Adds the ability to ping on the map to highlight points of interest."]
     (video-embed "https://gitlab.com/foundry-azzurite/pings/-/raw/master/doc/ping.mp4")]))

(rum/defc foundry-cursorhider-showcase < reactive []
  (module-showcase "Cursor Hider"
    [:<>
     [:p "Foundry shows everyone's mouse cursor position by default. The only option is to completely hide or "
      "show everyone's cursor. This module gives you the ability to selectively hide your individual cursor from "
      "other players, temporarily or permanently."]
     (video-embed "https://gitlab.com/foundry-azzurite/cursor-hider/-/raw/master/doc/cursor-hider.mp4")]))

(rum/defc homepage < reactive []
  (page
    [:<>
     (welcome)
     (container
       [:h1 (use-style {:margin-top (rem 3)
                        :font-size (em 1.1)
                        :text-align "center"})
        "What exactly am I creating? See for yourself..."])
     (mapping-dragons-showcase)
     (container-accented
       [:<>
        (heading-with-accent :h2 "Extensions" "FoundryVTT")
        [:p (link "https://foundryvtt.com/" "FoundryVTT" {:in-new-tab true}) " is a modern and very customizable "
         "virtual tabletop where you can play games like Dungeons & Dragons on. I create modules for FoundryVTT that "
         "add or change functionality."]
        (foundry-pings-showcase)
        (foundry-cursorhider-showcase)])
     (container
      [:<>
       [:h2 "... and I want to do more of this!"]
       [:p (use-style {:color (::accent colors)
                       :font-weight "bold"})
        "Please consider supporting me financially to allow me to spend more time on these tools."]
       [:p "My current most important goal in life is to be able to do what I love and still earn enough to live."
        [:span.offhand (use-style {:font-size (px 8)}) "(Whose isn't?)"]
        "I want to be able to reduce the hours I have to do at my day job until I can quit it. Even $1 could help me "
        "with that."]])]))
