(ns azzurite.web.config
  (:require [aero.core :as aero]
            [clojure.java.io :as io]
            [integrant.core :as ig]))

((fn add-integrant-readers-to-aero []
   (defmethod aero/reader 'ig/ref [_ _ key] (ig/ref key))
   (defmethod aero/reader 'ig/refset [_ _ key] (ig/refset key))))

(defn read-config [resource-path] (-> (io/resource resource-path)
                                    aero/read-config))
