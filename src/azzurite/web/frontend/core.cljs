(ns azzurite.web.frontend.core
  (:require [rum.core :as rum]
            [azzurite.web.content.homepage :refer [homepage]]
            [azzurite.web.frontend.global-styles :refer [define-global-styles]]
            [stylefy.core :as stylefy]
            [stylefy.rum]))


(defn render []
  (rum/hydrate (homepage) (.getElementById js/document "content")))


(defn init []
  (stylefy/init {:dom (stylefy.rum/init)})
  (define-global-styles)
  (render))


