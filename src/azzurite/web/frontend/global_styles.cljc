(ns azzurite.web.frontend.global-styles
  (:require [stylefy.core :refer [tag]]))


(defn define-global-styles []
  (tag "p" {:text-align "justify"}))
