(ns azzurite.web.page-handler
  (:require [clojure.spec.alpha :as s]
            [azzurite.web.content.homepage :refer [homepage]]
            [azzurite.web.frontend.global-styles :refer [define-global-styles]]
            [rum.core :as rum]
            [stylefy.core :as stylefy :refer [use-style]]
            [integrant.core :as ig]))

(rum/defc head []
  [:head [:title "Azzurite's Code"]
   [:link {:rel "stylesheet"
           :href "https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
           :integrity "sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w=="
           :crossorigin "anonymous"}]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
   [:style {:id "_stylefy-server-styles_"} "_stylefy-server-styles-content_"]
   [:style {:id "_stylefy-constant-styles_"}]
   [:style {:id "_stylefy-styles_"}]])

(rum/defc html-page [content]
  [:html {:lang "en"}
   (head)
   [:body
    [:#content content]
    [:script {:src "/js/site.js"}]]])

(defn create-html-response-handler [body-render-fn]
  (fn [request]
    {:status 200
     :headers {"Content-Type" "text/html"}
     :body (str "<!DOCTYPE HTML>"
             (stylefy/query-with-styles
               (fn []
                 (define-global-styles)
                 (rum/render-html (html-page (body-render-fn))))))}))

(defmethod ig/init-key ::homepage [_ _] (create-html-response-handler homepage))
