(ns azzurite.web.site
  (:require [integrant.core :as ig]
            [reitit.ring :as reitit]
            [stylefy.core :as stylefy]))

(defmethod ig/init-key ::resource-handler
  [_ opts]
  (reitit/create-resource-handler opts))

(defmethod ig/init-key :reitit.ring/redirect-trailing-slash-handler
  [_ opts]
  (reitit/redirect-trailing-slash-handler opts))

(defmethod ig/init-key :reitit.ring/default-handler
  [_ opts]
  (reitit/create-default-handler opts))

(defmethod ig/init-key ::default-multi-handler
  [_ handlers]
  (apply reitit/routes handlers))

(defmethod ig/init-key ::routes
  [_ routes]
  routes)

(defmethod ig/init-key ::handler
  [_ {:keys [routes default-handler]}]
  (reitit/ring-handler
    (reitit/router routes)
    default-handler))

(defmethod ig/init-key ::stylefy
  [_ opts]
  (stylefy/init opts))
(defmethod ig/suspend-key! ::stylefy [_ _] nil)
(defmethod ig/resume-key ::stylefy [_ _ _ old-impl] old-impl)
