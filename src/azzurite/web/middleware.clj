(ns azzurite.web.middleware
  (:require [integrant.core :as ig]
            [ring.middleware.defaults :as ring]))

(defmethod ig/init-key ::site-defaults
  [_ opts]
  (fn [handler]
    (ring/wrap-defaults handler (merge ring/site-defaults opts))))
