(ns azzurite.web.core
  (:require [integrant.core :as ig]
            [azzurite.web.config :refer [read-config]]))

(def config (read-config "config.edn"))

(defn -main [& opts]
  (ig/load-namespaces config)
  (ig/init config))
