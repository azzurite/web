(ns azzurite.web.ring-adapter
  (:require [integrant.core :as ig]
            [clojure.spec.alpha :as s]
            [ring.adapter.jetty :as jetty]))

(s/def ::port pos-int?)
(defmethod ig/pre-init-spec ::jetty [_]
  (s/keys :req-un [::port]))

(defmethod ig/init-key ::jetty
  [_ {:keys [port handler]}]
  (jetty/run-jetty handler {:join? false
                            :port port}))
(defmethod ig/halt-key! ::jetty
  [_ server]
  (.stop server))
